const express = require('express')
const router = express.Router()
module.exports = router

router.get('/', (req, res) => {
  const file = "assets/data.xml";
  res.download(file); // Set disposition and send it.
});
