const express = require('express')
const router = express.Router()
module.exports = router
var fs = require("fs"),
  parseString = require("xml2js").parseString;

// index page
router.get('/', function(req, res) {
  fs.readFile("assets/gdp.xml", "utf-8", function(err, data) {
    if (err) console.log(err);
    // we log out the readFile results
    // we then pass the data to our method here
    parseString(data, function(err, result) {
      if (err) console.log(err);
      // here we log the results of our xml string conversion
      var year = [ ]
      var gdp = [ ]
      for (var i = 0; i < result.data.record.length; i++) {
        year.push(result.data.record[i].field[2]._);
        gdp.push(result.data.record[i].field[3]._);
      }
      res.json({
        year: year,
        gdp: gdp
      });
    });
  });
});
