const express = require('express')
const router = express.Router()
module.exports = router
var fs = require("fs"), parseString = require("xml2js").parseString
var builder = require('xmlbuilder');



router.get('/', function(req, res) {

  fs.readFile("assets/gdp.xml", "utf-8", function(err, data_gdp) {
    if (err) console.log(err);

    // we then pass the data to our method here
    parseString(data_gdp, function(err, result_gdp) {
      if (err) console.log(err);

      // add data gdp to array
      var year = [ ]
      var gdp = [ ]

      for (var i = 0; i < result_gdp.data.record.length; i++) {
        year.push(result_gdp.data.record[i].field[2]._);
        gdp.push(result_gdp.data.record[i].field[3]._);
      }

      // read xml gni
      fs.readFile("assets/gni.xml", "utf-8", function(err, data_gni) {
        if (err) console.log(err);

        // we then pass the data to our method here
        parseString(data_gni, function(err, result_gni) {
          if (err) console.log(err);

          var gni = [ ]
          for (var i = 0; i < result_gni.data.record.length; i++) {
            gni.push(result_gni.data.record[i].field[3]._);


          }



          //console.log(year);
          //console.log(gdp);
          //console.log(gni);



          var root = builder.create('squares');
          root.com('f(x) = x^2');
          for(var i = 1; i <= 7; i++){

            var ele = root.ele('data_gdp')
              .ele('gdp').up()
              .end();

            var ele = root.ele('data_gni')
              .ele('gni').up()
              .end();

          }





          var xml = root.end({ pretty: true});
          console.log(xml);


          fs.writeFile("assets/edited-test.xml", xml, function(err, data) {
            if (err) console.log(err);

            console.log("successfully written our update xml to file");
          });

          res.json({
            year: year,
            gdp: gdp,
            gni: gni
          });


        });
      });
    });
  });
});
