const express = require('express')
const router = express.Router()
module.exports = router


router.get('/', (req, res) => {
  res.send("<h1>Home API</h1>")
})

router.use('/gdp', require('./gdp'))
router.use('/gni', require('./gni'))
router.use('/download', require('./download_xml'))
router.use('/upload', require('./write_xml'))
