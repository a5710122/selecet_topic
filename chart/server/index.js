// load module
let compression = require('compression')
let express = require('express')
let bodyParser = require('body-parser')

// PORT
const port = 8000;

// Call API
const route_api = require('./api')

// Create Instance express
const app = express()

// Call BodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// Call compression
app.use(compression({
  filter: (req, res) => (!req.headers['x-no-compression'] && compression.filter(req, res)) || false,
}))

app.use(express.static('public'))

// Cal CORS
app.use((req, res, next) => {
  var header = { 'Access-Control-Allow-Origin': '*' }
  for (var i in req.headers) {
    if (i.toLowerCase().substr(0, 15) === 'access-control-') {
      header[i.replace(/-request-/g, '-allow-')] = req.headers[i]
    }
  }
  res.header(header)
  next()
})

// Use API
app.use('/api', require('./api'))

app.get('/', (req, res) => {
  try {
    return res.status(200).send("Hello");
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'ไม่สามารถเชื่อมต่อได้'
    });
  }
})

// PORT
app.listen(port, () => {
  console.log(`Start server at port ${port}.`)
})
