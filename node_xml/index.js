var express = require('express');
var app = express();

const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

var fs = require("fs"),
  parseString = require("xml2js").parseString;

// set the view engine to ejs
app.set('view engine', 'ejs');

var year = [ ]
var gdp = [ ]

// index page
app.get('/', function(req, res) {
  fs.readFile("data.xml", "utf-8", function(err, data) {
    if (err) console.log(err);
    // we log out the readFile results
    // we then pass the data to our method here
    parseString(data, function(err, result) {
      if (err) console.log(err);
      // here we log the results of our xml string conversion
      for (var i = 0; i < result.data.record.length; i++) {
        year.push(result.data.record[i].field[2]._);
        gdp.push(result.data.record[i].field[3]._);
      }
    });
  });
    res.render('pages/index', {
      year: year,
      gdp: gdp
    });
});


app.listen(8080);
console.log('8080 is the magic port');
